package com.westpac.qa.TestBase;

/**
 * @author venkatapavan.kumar
 * scenario 		 : To calculate savings bank Interest
 * Test Case Name    : PersonalTab 
 * Test Description  : Savings Bank Calculator 
 * Date of creation  : 18-July-2019
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.westpac.qa.Util.Testutil;

public class TestBase {

	public static WebDriver driver;
	public static Properties prop;

	public TestBase() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					"C:\\Users\\venkatapavan.kumar\\eclipse-workspace\\com.westpac.NZ\\"
							+ "src\\main\\java\\com\\westpac\\qa\\config\\config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

//	Initialization of Browser
	public static void intilization() {
		String browserName = prop.getProperty("browser");
		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
			driver = new ChromeDriver();

		} else if (browserName.equals("FF")) {
			System.setProperty("webdriver.gecko.driver",
					"C:\\Users\\venkatapavan.kumar\\Documents\\Practice Related Tasks\\Internal Apps\\selenium-java-3.141.59\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(Testutil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(Testutil.IMPLICITWAIT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));

	}

}
