package com.westpac.qa.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.westpac.qa.TestBase.TestBase;


public class HomePage extends TestBase{
	
	@FindBy(xpath="//*[@class='sw-header-menu']/li/a[contains(text(),'Personal')]")
	WebElement Personaltab;
	
	
	@FindBy(xpath="//*[@class='sw-header-menu']/li/a[contains(text(),'Business')]")
	WebElement Businesstab;
	
	
	@FindBy(xpath="//*[@class='sw-header-menu']/li/a[contains(text(),'Agribusiness')]")
	WebElement Agribusinesstab;

	@FindBy(xpath="//*[@class='sw-header-menu']/li/a[contains(text(),'Institutional')]")
	WebElement Institutionaltab;
	
	@FindBy(xpath="//*[@class='sw-header-menu']/li/a[contains(text(),'Who We Are')]")
	WebElement WhoWeAretab;
	
	@FindBy(xpath="//*[@class='sw-header-menu']/li/a[contains(text(),'Growing NZ')]")
	WebElement GrowingNZtab;
	
	@FindBy(xpath="//*[@id='logo']")
	WebElement NZlogo;
	
	
	public HomePage() {
		
		PageFactory.initElements(driver, this);
				
	}
	
    public String ValidPageTitle() {
        return driver.getTitle();
 }
 
 
 public boolean ValidateNZLogo() {
        return NZlogo.isDisplayed();
        
 }  
        public void clickbusinesstab() {
        	  Businesstab.click();
        }
        
        
        public void clickAgribusinesstab() {
        	Agribusinesstab.click();
      }
        
        public void clickInstitutionaltab() {
        	Institutionaltab.click();
      }
        
        public void clickWhoWeAretab() {
        	WhoWeAretab.click();
      }
        
        
        public void clickGrowingNZtab() {
        	GrowingNZtab.click();
      }

}
