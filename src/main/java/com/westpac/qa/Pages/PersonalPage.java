package com.westpac.qa.Pages;

import java.util.List;

/**
 * @author venkatapavan.kumar
 * scenario 		 : To calculate savings bank Interest
 * Test Case Name    : PersonalTab 
 * Test Description  : Savings Bank Calculator 
 * Date of creation  : 19-July-2019
 */

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.westpac.qa.TestBase.TestBase;
import com.westpac.qa.Util.Testutil;

public class PersonalPage extends TestBase {

	@FindBy(xpath = "//*[@id='header-ps']/div/nav[2]/ul/li/a[contains(text(),'Personal')]")
	WebElement Personaltab;

	@FindBy(xpath = "//*[@class='sw-ubermenu-sections']/li/a[contains(text(),'Investments')]")
	WebElement Investmentstab;

	@FindBy(xpath = "//*[@id='sidenav-responsive-children-title-1473-ps']//span[2]")
	WebElement CalculatorsButton;

	@FindBy(xpath = "//h2[@class='sw-page-item-heading sw-page-item-heading-list']/a[@title='Which could give the best return?']")
	WebElement BestReturnLink;

	@FindBy(xpath = "//span[contains(text(),'Savings Account')]")
	WebElement SavingsAccountCalc;

	@FindBy(xpath = "//*[@id='wpc-amount-to-save-input-1']")
	WebElement CalcAmountInput;

	@FindBy(xpath = "//*[@id='wpc-ongoing-savings-input-1']")
	WebElement CalcOngoingSvgsInput;

	@FindBy(xpath = "//*[@id=\'wpc-ongoing-savings-frequency-1\']")
	WebElement CalcOngoingSvgsFreq;

	@FindBy(xpath = "//*[@id='wpc-investment-term-years-input-1']")
	WebElement CalcSvgsPeriodYearsInput;

	@FindBy(xpath = "//*[@id='wpc-investment-term-months-input-1']")
	WebElement CalcSvgsPeriodMonthsInput;

	@FindBy(xpath = "//*[@id='wpc-investment-term-days-input-1']")
	WebElement CalcSvgsPeriodDaysInput;

	@FindBy(xpath = "//*[@id='wpc-interest-rate-input-1']")
	WebElement CalcInterestRateInput;

	@FindBy(xpath = "//*[@id='wpc-interest-payment-frequency-1']")
	WebElement CalcInterestPaidInput;

	@FindBy(xpath = "//*[@id='uniform-cl-income-tax-rate-1']")
	WebElement CalcIncomeTaxRateInput;

	@FindBy(xpath = "//*[@id='cl-income-tax-1']/div/div/a")
	WebElement CalcFindMyRateButton;
	// "//*[@id='cl-income-tax-1']//div/label/following-sibling::a[contains(text(),'Find
	// My Rate')]")
	/* End of xpaths of all the Calculator fields */

	@FindBy(xpath = "//*[@id='tax-calc-q1']/td[2]/label[1]/input")
	WebElement NZtaxresident;

	@FindBy(xpath = "//*[@id='tax-calc-q2']/td[2]/label[1]/input")
	WebElement Myinvestmentsheldby;

	@FindBy(xpath = "//*[@id='tax-calc-q3']/label[1]/input")
	WebElement annualtaxable;

	@FindBy(xpath = "//*[@id=\'tax-calc-income-result\']/h3")
	WebElement prescribedIncomeTaxRate;

	@FindBy(xpath = "//*[@id='cl-modal-close-button']")
	WebElement closebutton;

	@FindBy(xpath = "//div[@id='cl-summary-row-1']//div/table/tbody/tr[3]")
	WebElement SummaryData;

	public PersonalPage() {
		PageFactory.initElements(driver, this);
	}

	public String ValidPageTitle() {
		return driver.getTitle();
	}

	public boolean validPersonaltab() {
		return Personaltab.isDisplayed();

	}

	public void clickoninvestmentpage() {
		Actions action = new Actions(driver);
		action.moveToElement(Investmentstab).click().perform();
		System.out.println("clicked on investment tab ");
	}

	public void scrolldown() {
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		js.executeScript("window.scrollBy(0,200)");
	}

	public void clickonCalculatorlink() {
		driver.manage().timeouts().implicitlyWait(Testutil.IMPLICITWAIT, TimeUnit.SECONDS);
		Actions action1 = new Actions(driver);
		action1.moveToElement(CalculatorsButton).click().perform();
		System.out.println("clicked on Calculator button tab ");
	}

	public void clickonbestretuntitlelink() {
		BestReturnLink.click();
	}

	public void clickonSavingsAccountCalclink() {
		SavingsAccountCalc.click();
	}

	public void enterdatatosavingcalc(String IntialDepsoit, String Ongoingsaving, String OngoingSavingFreq,
			String Savingyear, String Savingmonth, String Intrestrate, String intrestpaid, String IncomeTaxRate)
			throws InterruptedException {
		
		
		//To capture and validate all the information icons present for all the 6 fields in the Savings Bank Calculator 
		
		List<WebElement> element = driver.findElements(By.xpath("//*[@class='wpc-input-label']/following-sibling::div/a[@class='ir wpc-icon wpc-icon-help']"));
        
		
        for(int i=0; i< element.size(); i++) {
	        WebElement tooltip= element.get(i);
	        
	        if (tooltip.isDisplayed()) {                   
	        	String toolTipText = tooltip.getAttribute("original-title");
	        	System.out.println("Tool Tip "+i+" : "+toolTipText);
	        }
        }
		
		CalcAmountInput.sendKeys(Keys.CONTROL + "a");
		CalcAmountInput.sendKeys(Keys.DELETE);
		CalcAmountInput.sendKeys(IntialDepsoit);
		CalcOngoingSvgsInput.sendKeys(Ongoingsaving);

		Select select2 = new Select(CalcOngoingSvgsFreq);
		select2.selectByVisibleText(OngoingSavingFreq);

		CalcSvgsPeriodYearsInput.sendKeys(Savingyear);
		CalcSvgsPeriodMonthsInput.sendKeys(Savingmonth);
		// CalcSvgsPeriodDaysInput.clear();
		// CalcSvgsPeriodDaysInput.sendKeys("1");
		CalcInterestRateInput.sendKeys(Keys.CONTROL + "a");
		CalcInterestRateInput.sendKeys(Keys.DELETE);
		CalcInterestRateInput.sendKeys(String.valueOf(Intrestrate));
        Thread.sleep(3000);
		scrolldown();

		Select select = new Select(CalcInterestPaidInput);
		select.selectByVisibleText(intrestpaid);
//               IncomeTaxRate
		CalcIncomeTaxRateInput.click();
//               Select select1 = new Select(CalcIncomeTaxRateInput);
//               select1.selectByValue(IncomeTaxRate);
		Actions keyDown = new Actions(driver);
		keyDown.sendKeys(Keys.chord(Keys.DOWN, Keys.DOWN)).perform();
//		Thread.sleep(6000);
		
		CalcFindMyRateButton.click();
		CalcFindMyRateButton.click();
		Thread.sleep(3000);
		System.out.println("Clicked on Find My Rate Button");

		NZtaxresident.click();
		Myinvestmentsheldby.click();
		annualtaxable.click();

		Thread.sleep(5000);

		String prescribedIncomeTaxRateV;
		prescribedIncomeTaxRateV = prescribedIncomeTaxRate.getText();
		System.out.println("The Prescribed Income Tax Rate is : " + prescribedIncomeTaxRateV);

		closebutton.click();
		System.out.println("sucessfully updated the saving account");

		String SummaryD = SummaryData.getText();
		System.out.println("Here is the Final Summary:" + SummaryD);

	}

}