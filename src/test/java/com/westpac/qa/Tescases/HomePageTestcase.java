package com.westpac.qa.Tescases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.westpac.qa.Pages.HomePage;
import com.westpac.qa.TestBase.TestBase;

public class HomePageTestcase extends TestBase{
	
    HomePage homePage;
    
    public HomePageTestcase() {
           super();
          
    }
    
    @BeforeMethod
    public  void setup() {
         intilization();
                           
    }
    
    
    @Test
   public void Verifypagetitle() {
    	try {
    		 Thread.sleep(5000);
			String htitle = homePage.ValidPageTitle();
			Assert.assertEquals("htitle",
					"Bank | Westpac New Zealand - Helping Kiwis with their personal banking");
		} catch (Exception e) {
			
		} 
   	
    
    }
    
    
	/*
	 * @Test public void validatelogo() { try { Thread.sleep(5000);
	 * 
	 * homePage.ValidateNZLogo(); } catch (InterruptedException e) {
	 * 
	 * e.printStackTrace(); }
	 * 
	 * 
	 * }
	 */

    @Test
    public void clickonpersonaltab() throws InterruptedException {
   	 Thread.sleep(5000);
   	 try {
   		 
 
   		homePage.clickAgribusinesstab();
   		homePage.clickbusinesstab();
   		homePage.clickGrowingNZtab();
   		homePage.clickInstitutionaltab();
   		homePage.clickWhoWeAretab();
   	} catch (Exception e) {
   		// TODO: handle exception
   	}
   	 
    }
//    
    
    @AfterMethod 
    public void teardown() {
           driver.quit();
    }
    


}
