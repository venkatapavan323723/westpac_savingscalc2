package com.westpac.qa.Tescases;
/**
 * @author venkatapavan.kumar
 * scenario 		 : To calculate savings bank Interest
 * Test Case Name    : PersonalTab 
 * Test Description  : Savings Bank Calculator 
 * Date of creation  : 19-July-2019
 */

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import org.testng.annotations.Test;

import com.westpac.qa.Pages.PersonalPage;
import com.westpac.qa.TestBase.TestBase;
import com.westpac.qa.Util.Testutil;

public class PersonaltabTestcase extends TestBase{
	
	PersonalPage PersonalPage;
    Testutil TestUtil;
    String sheetName = "Savings";

    public PersonaltabTestcase() {
    	super();
    }

    @BeforeMethod
    public void setup() {
    	intilization();
    	PersonalPage = new PersonalPage();
    }

    @DataProvider
    public Object[][] getSavingTestData() {
    	Object data[][] = Testutil.getTestData(sheetName);
        return data;
    }

    @Test(priority = 1, dataProvider = "getSavingTestData")
    public void enterdatatosavingcalc(String IntialDepsoit, String Ongoingsaving, String OngoingSavingFreq, String Savingyear, String Savingmonth,
        String Intrestrate, String intrestpaid, String IncomeTaxRate) throws Exception {
    	Thread.sleep(3000);
        String title = PersonalPage.ValidPageTitle(); 
        System.out.println("titile is " + title);
        Boolean flag = PersonalPage.validPersonaltab();
        Assert.assertTrue(flag);
        PersonalPage.clickoninvestmentpage();
        PersonalPage.scrolldown();
        PersonalPage.clickonCalculatorlink();
        driver.manage().timeouts().implicitlyWait(Testutil.IMPLICITWAIT, TimeUnit.SECONDS);
        PersonalPage.clickonbestretuntitlelink();
        driver.manage().timeouts().implicitlyWait(Testutil.IMPLICITWAIT, TimeUnit.SECONDS);
        PersonalPage.clickonSavingsAccountCalclink();
        driver.manage().timeouts().implicitlyWait(Testutil.IMPLICITWAIT, TimeUnit.SECONDS);
        PersonalPage.enterdatatosavingcalc(IntialDepsoit, Ongoingsaving, OngoingSavingFreq, Savingyear, Savingmonth, Intrestrate,intrestpaid, IncomeTaxRate);
    }

        @AfterMethod
        public void teardown() {
        	if (driver != null) 
        		driver.quit();
        }
}
